from flask import Flask, url_for, session, jsonify
from flask import render_template, redirect
from authlib.integrations.flask_client import OAuth


app = Flask(__name__)
app.secret_key = '!secret'
app.config.from_object('config')

oauth = OAuth(app)
oauth.register(
    name='wafeq',
    access_token_url='https://app.wafeq.com/oauth/token/',
    refresh_token_url='https://app.wafeq.com/oauth/token/',
    authorize_url='https://app.wafeq.com/oauth/authorize/',
    fetch_token=lambda: session.get('token'),
    api_base_url='https://api.wafeq.com/v1/',
    client_kwargs={'scope': 'basic'},
)


@app.route('/')
def homepage():
    return render_template('home.html', token=session.get('token'))


@app.route('/login')
def login():
    redirect_uri = url_for('auth', _external=True)
    return oauth.wafeq.authorize_redirect(redirect_uri)


@app.route('/logout')
def logout():
    session.pop('token')
    return redirect('/')


@app.route('/auth')
def auth():
    token = oauth.wafeq.authorize_access_token()
    session['token'] = token
    return redirect('/')


@app.route('/organization')
def organization():
    response = oauth.wafeq.get('organization/')
    return jsonify(response.json())
